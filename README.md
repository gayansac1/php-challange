# Mighty Mind PHP/SilverStripe/GraphQL Challenge

***Install Docker engine***

[Docker Install Documentation](https://docs.docker.com/engine/install/)

When Writing your code, please be mindful of the following:

-use php code sniffer for php linting
-use editorconfig for editor configuration
-use composer to unstall new libraries

The code must be submitted to public repo in gitlab and a live version has to be deployed on gitlab pages

Please add the public link to the README.md file on your codebase, and any other instructions about how to run the code

***Installation***

Install Docker on your machine.

Clone this repo on your local machine.


```

## Usage

# Get into the cloned repo folder
cd PHPChallenge/

# run docker-compose to spinup the environments
~/PHPChallenge/$  docker-compose up -d

# Reach the mightyminds Silverstripe document root
cd MightyMindsRoot/MightyMinds/

# Install the Libraries
composer install

~/PHPChallenge/MightyMindsRoot/MightyMinds/$ composer install

# .env file it is required by silverstripe to connect to the proper resources.

```


***DB credentials***

SS_DATABASE_CLASS="MySQLDatabase"

SS_DATABASE_SERVER="172.22.0.2"

SS_DATABASE_USERNAME="root"

SS_DATABASE_PASSWORD="domenico123"

SS_DATABASE_PORT="3306"

SS_DATABASE_NAME="mightymindsdb"

SS_DEFAULT_ADMIN_USERNAME="admin"

SS_DEFAULT_ADMIN_PASSWORD="domenico123"




 **FYI: Replase your domain name with '$yourDomain.com' to run the below URLs**

***Build the DataBase and Flush cache***

After you create the '.env' run below URL for create your Database Tables and links, and clear the cache.
 
$yourDomain.com/dev/build?flush=all


***public urls***

You can use below URLs to query the data from the objects each objects

***member*** <br />
$yourDomain.com/graphql/?query={readMembers{ID+FirstName+Email}}

***pages*** <br />
$yourDomain.com/graphql/?query={readHome{ID+Title+Content}}

***teachers*** <br />
$yourDomain.com/graphql/?query={readTeachers{StaffID+FirstName}}