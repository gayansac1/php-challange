<?php

namespace MightyMind\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use MightyMind\Models\Teacher;
use SilverStripe\GraphQL\OperationResolver;
use SilverStripe\GraphQL\QueryCreator;

class ReadTeachersQueryCreator extends QueryCreator implements OperationResolver
{
    public function attributes()
    {
        return [
            'name' => 'readTeachers'
        ];
    }

    public function args()
    {
        return [
            'StaffID' => ['type' => Type::string()]
        ];
    }

    public function type()
    {
        return Type::listOf($this->manager->getType('teachers'));
    }

    public function resolve($object, array $args, $context, ResolveInfo $info)
    {
        $list = Teacher::get();

        if (isset($args['StaffID'])) {
            $list = $list->filter('StaffID', $args['StaffID']);
        }

        return $list;
    }

}