<?php

namespace MightyMind\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class TeachersTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'teachers'
        ];
    }

    public function fields()
    {
        return [
            'StaffID' => ['type' => Type::nonNull(Type::id())],
            'FirstName' => ['type' => Type::string()],
            'LastName' => ['type' => Type::string()]
        ];
    }

}