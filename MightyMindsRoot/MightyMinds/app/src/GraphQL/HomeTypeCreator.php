<?php

namespace MightyMind\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\Pagination\Connection;
use SilverStripe\GraphQL\TypeCreator;

class HomeTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'home'
        ];
    }

    public function fields()
    {

        return [
            'ID' => ['type' => Type::nonNull(Type::id())],
            'Title' => ['type' => Type::string()],
            'Content' => ['type' => Type::string()],
            'URLSegment' => ['type' => Type::string()],
            'LinkID' => ['type' => Type::string()]
        ];
    }

}