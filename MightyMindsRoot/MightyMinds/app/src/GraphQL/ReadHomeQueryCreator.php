<?php

namespace MightyMind\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\OperationResolver;
use SilverStripe\GraphQL\QueryCreator;

class ReadHomeQueryCreator extends QueryCreator implements OperationResolver
{
    public function attributes()
    {
        return [
            'name' => 'readHome'
        ];
    }

    public function args()
    {
        return [
            'Title' => ['type' => Type::string()]
        ];
    }

    public function type()
    {
        return Type::listOf($this->manager->getType('home'));
    }

    public function resolve($object, array $args, $context, ResolveInfo $info)
    {
        $list = \Page::get();

        if (isset($args['Title'])) {
            $list = $list->filter('Title', $args['Title']);
        }

        return $list;
    }

}