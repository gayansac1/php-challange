<?php

namespace MightyMind\Models;

use SilverStripe\ORM\DataObject;

class Teacher extends DataObject
{
    private static $table_name = 'Teacher';

    private static $db = [
        'StaffID'        => 'Int',
        'FirstName' => 'Varchar',
        'LastName'  => 'Varchar'
    ];

    private static $has_one = [
        'Page'  => \Page::class
    ];

    public function getCMSFields()
    {
        $fiels = parent::getCMSFields();

        $fiels->removeByName([
            'Sort',
            'PageID'
        ]);

        return $fiels;
    }
}