<?php

namespace {

    use MightyMind\Models\Teacher;
    use Sheadawson\Linkable\Forms\LinkField;
    use Sheadawson\Linkable\Models\Link;
    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\GridField\GridField;
    use SilverStripe\Forms\GridField\GridFieldAddNewButton;
    use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

    class Page extends SiteTree
    {
        private static $db = [];

        private static $has_one = [
            'Link' => Link::class
        ];

        private static $has_many = [
            'Teachers'  => Teacher::class
        ];

        public function getCMSFields()
        {
            $fiels = parent::getCMSFields();

            $fiels->addFieldsToTab('Root.Info',[
                LinkField::create('LinkID', 'Link'),
                $teachers = GridField::create('Teachers')
            ]);
            $teachers->setList($this->Teachers());
            $teachers->setConfig($gridConfig = GridFieldConfig_RecordEditor::create());
            $gridConfig->getComponentByType(GridFieldAddNewButton::class)
                ->setButtonName('Add Teacher');


            return $fiels;
        }

    }
}
